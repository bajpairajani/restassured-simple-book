import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import static io.restassured.RestAssured.*;

public class GETStatus {
	@BeforeClass
	public void setUp() {
		RestAssured.baseURI = "https://simple-books-api.glitch.me";
	}
	@Test
	public void getStatus() {
		Response myResponse = RestAssured.get("/status");
		System.out.println(myResponse.getContentType());
		System.out.println(myResponse.getHeaders());
		//print response body
		System.out.println(myResponse.body());
		//print status code
		int statusCode = myResponse.getStatusCode();
		System.out.println(statusCode);
		Assert.assertEquals(statusCode, 200);
		/*this assertion will fail as this needs integer value as expected value
		 * Assert.assertEquals(statusCode, "200");
		 */
		
		/*This will fail as the status code does not match
		 * Assert.assertEquals(statusCode, 201);
		 * */
		
	}
	@Test
	public void getSingleBookDetail() {
		//as we include io.restassured.RestAssured.* so we don't need RestAssured object
		Response regClientResponse = RestAssured.get("/books/1");
		System.out.println(regClientResponse.asString());
		System.out.println(regClientResponse.getStatusCode());
		System.out.println(regClientResponse.getContentType());
		
	}
	@Test
	public void givenThen() {
		given().get("/status").then().statusCode(200);
		
	}

}
